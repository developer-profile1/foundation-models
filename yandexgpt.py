import os
import time
import grpc
# import yandexgpt
from langchain_community.llms import YandexGPT

FOLDER_ID = os.getenv("YC_FOLDER_ID")
IAM_TOKEN = os.getenv("YC_IAM_TOKEN")

for attempt, temperature in enumerate([0.0, 0.0, 0.5, 0.5, 1.0, 0.5, 0.0]):
    llm = YandexGPT(
        model_name='yandexgpt',
        model_vesrsion='latest',
        # model_uri='gpt://b1g9u87m9ljh5e6tvt8i/yandexgpt/latest',
        folder_id=FOLDER_ID,
        iam_token=IAM_TOKEN,
        disable_request_logging=True,
        temparature=temperature
    )

    print(
        f'''Attempt: {attempt}, temperature: {temperature}.\n Answer: {llm.invoke('Hello world, how are you?')}'''
    )
    time.sleep(1)